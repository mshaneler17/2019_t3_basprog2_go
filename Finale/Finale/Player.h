#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<time.h>
#include"Pokemon.h"


using namespace std;

class Player
{
public:
	Player();
	Player(string Name);

	string name;
	int currency;
	int pokeballCount;
	int xPosition;
	int yPosition;
	bool posSafe;

	void displayPlayer();
	void currentLocation(string &name, bool &safe, vector<Pokemon> & list);
	void move(int &deadEnd);

	void pokeMart();
	void pokemonCenter(vector<Pokemon> & list);

	void encounter(vector<Pokemon> & list, Pokemon * target);
	void pokemonSwitch(vector<Pokemon> & list);
	void partyGainExperience(vector<Pokemon> & list);
	void addParty(vector<Pokemon> & list, Pokemon * target);
	void displayParty(vector<Pokemon> & list);
};