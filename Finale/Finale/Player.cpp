#include "Player.h"

Player::Player()
{
	name = "";
	xPosition = 0;
	yPosition = 0;
	posSafe = true;

	currency = 0;
	pokeballCount = 0;
}

Player::Player(string Name)
{
	name = Name;
	xPosition = 0;
	yPosition = 0;
	posSafe = true;

	currency = 500;
	pokeballCount = 6;
}

void Player::displayPlayer()
{
	system("CLS");
	cout << "Name: " << this->name << endl;
	cout << "Money: " << this->currency << endl;
	cout << "Pokeballs: " << this->pokeballCount << endl;
	system("PAUSE");
}

void Player::currentLocation(string & name, bool & safe, vector<Pokemon> & list)
{
	system("CLS");
	string answer = "";
	posSafe = safe;
	cout << endl;
	cout << "You are here in " << name << endl;
	cout << "Current position: X = " << xPosition << " || Y = " << yPosition << endl;
	system("PAUSE");

	if (posSafe == true)
	{
		while (true)
		{
			cout << endl;
			cout << "This is a safe place" << endl;
			cout << "Choose from the three on what to do?" << endl;
			cout << "[1] Pokemon Center || [2] Pokemart || [3] Check Party || [Any key] move" << endl;
			cin >> answer;

			if (answer == "1")
			{
				pokemonCenter(list);
			}
			else if (answer == "2")
			{
				pokeMart();
			}
			else if (answer == "3")
			{
				displayParty(list);
			}
			else
			{
				break;
			}
		}
	}
}

void Player::move(int & deadEnd)
{
	system("CLS");
	string answer;
	cout << endl;
	cout << "To move: [w] to North, [s] to South, [a] to West, [d] to East" << endl;
	cout << "Answer: ";
	cin >> answer;

	if (answer == "w")
	{
		if (yPosition < deadEnd)
		{
			this->yPosition++;
		}
		else
		{
			cout << "Cannot move there" << endl;
			system("PAUSE");
		}
	}
	else if (answer == "s")
	{
		if (yPosition > 0)
		{
			this->yPosition--;
		}
		else
		{
			cout << "Cannot move there" << endl;
			system("PAUSE");
		}
	}
	else if (answer == "a")
	{
		if (xPosition > 0)
		{
			this->xPosition--;
		}
		else
		{
			cout << "Cannot move there" << endl;
			system("PAUSE");
		}
	}
	else if (answer == "d")
	{
		if (xPosition < 6)
		{
			this->xPosition++;
		}
		else
		{
			cout << "Cannot move there" << endl;
			system("PAUSE");
		}
	}
	else
	{
		cout << "Cannot move there" << endl;
		system("PAUSE");
	}
}

void Player::pokeMart()
{
	string choice;
	while (choice != "1")
	{
		system("CLS");
		cout << endl;
		cout << "Welcome to 9/11 store, may I help you?" << endl;
		cout << "[1] buy pokeballs || [any key] leave" << endl;
		cin >> choice;

		if (choice == "1")
		{
			int count = -1;
			while (count < 0)
			{
				system("CLS");
				cout << endl;
				cout << "How many pokeballs would you like to buy?" << endl;
				cout << "1 pokeball = $100. You currenly have $" << this->currency << endl;
				cout << "How many: ";
				cin >> count;
			}
			if (this->currency >= count * 100)
			{
				cout << "Thank you for the business" << endl;
				cout << "Pokeballs: " << this->pokeballCount << endl;
				cout << "Current budget: $" << this->currency << endl;
				system("PAUSE");
			}
			else
			{
				cout << "Not enough funds to purchase" << endl;
				system("PAUSE");
			}
		}
		else
		{
			cout << "Come back again" << endl;
			system("PAUSE");
			break;
		}
		system("PAUSE");
	}
}

void Player::pokemonCenter(vector<Pokemon> & list)
{
	cout << endl;
	cout << "Your Pokemon is healed" << endl;

	for (int i = 0; i < list.size(); i++)
	{
		list[i].heal();
	}
	system("PAUSE");
}

void Player::encounter(vector<Pokemon>& list, Pokemon * target)
{
	system("CLS");
	cout << endl;
	cout << this->name << "saw an enemy Pokemon " << target->name << endl;
	system("PAUSE");
	while (true)
	{
		string choice = "";
		system("CLS");
		cout << endl;

		list[0].pokemonStatView();
		target->pokemonStatView();

		cout << endl;
		cout << "What would you do?" << endl;
		cout << "[1] Attack || [2] Catch || [Any key] Flee" << endl;
		cout << "Choice: ";
		cin >> choice;

		if (choice == "1")
		{
			cout << "You chose attack" << endl;
			list[0].attack(target);
			system("PAUSE");
		}
		else if (choice == "2")
		{
			cout << "You chose catch" << endl;
			system("CLS");
			srand(time(0));
			int odds = rand() % 100;
			cout << endl;
			if (pokeballCount < 1)
			{
				cout << "You don't have any pokeballs" << endl;
			}
			else
			{
				pokeballCount--;
				cout << "You throw the pokeball" << endl;
				system("PAUSE");

				if (odds > 30)
				{
					cout << "You successfully caught " << target->name << endl;
					system("PAUSE");
					partyGainExperience(list);
					addParty(list, target);
					break;
				}
				else
				{
					cout << "You missed it" << endl;
				}
			}
		}
		else
		{
			cout << "You ran away" << endl;
			break;
		}

		if (target->currentHp > 0)
		{
			target->attack(&list[0]);
		}
		else
		{
			system("CLS");
			cout << endl;
			cout << target->name << " is fainted" << endl;
			system("PAUSE");
			partyGainExperience(list);
			break;
		}
		pokemonSwitch(list);

		if (list[0].currentHp <= 0)
		{
			system("CLS");
			cout << "All of your pokemon are fainted and rush to the pokemon center" << endl;
			system("PAUSE");
			pokemonCenter(list);
			break;
		}

		system("PAUSE");
	}
}

void Player::pokemonSwitch(vector<Pokemon>& list)
{
	if (list.size() > 1)
	{
		if (list[0].currentHp <= 0)
		{
			Pokemon temp = list[0];
			for (int i = 0; i < list.size() - 1; i++)
			{
				list[i] = list[i + 1];
			}
			list[list.size() - 1] = temp;
		}
	}
}

void Player::partyGainExperience(vector<Pokemon>& list)
{
	for (int i = 0; i < list.size(); i++)
	{
		list[i].getExperience();
	}
}

void Player::addParty(vector<Pokemon>& list, Pokemon * target)
{
	if (list.size() < 6)
	{
		cout << endl;
		cout << "Pokemon " << target->name << " was added to the party" << endl;
		list.push_back(*target);
	}
	else
	{
		cout << endl;
		cout << "Pokemon " << target->name << " cannot be added. It will transfer to the box" << endl;
	}
	system("PAUSE");

	displayParty(list);
}

void Player::displayParty(vector<Pokemon>& list)
{
	system("CLS");
	cout << "Current party: " << endl;
	for (int i = 0; i < list.size(); i++)
	{
		cout << endl;
		cout << list[i].name << " - Level " << list[i].level << endl;
		cout << list[i].type << " Type" << endl;
		cout << list[i].currentHp << " / " << list[i].maxHp << endl;
	}
	system("PAUSE");
}
