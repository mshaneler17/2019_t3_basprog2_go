#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<time.h>

using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string Name, string Type, int Level, bool CanEvolve, int EvolveLevel);

	string name;
	string type;
	int level;
	int maxHp;
	int currentHp;
	int goalExp;
	int exp;
	int expGrowth;

	bool canEvolve;
	int evolveLevel;

	int baseDamage;
	int Damagebonus(Pokemon* target);

	int addHp(int input);
	int addDamage(int input);
	int addExpRate(int input);

	int DamageTimesTwo();
	int DamageDividesTwo();
	void pokemonStatView();

	void attack(Pokemon* target);
	void heal();

	void getExperience();
	void levelUp();
};