#include<iostream>
#include<string>
#include<vector>
#include<time.h>
#include"Player.h"
using namespace std;

struct Area
{
	string name;
	int yStart;
	int yEnd;
	bool isSafe;
};
struct Pokedex
{
	string name;
	string type;
	bool canEvolve;
	int evolveRequirement;
};

vector<Area> map;
vector<Pokedex> pokedex;
vector<string> typeList;

void typeListCreation(vector<string> &list)
{
	list.push_back("Normal");//0
	list.push_back("Fighting");
	list.push_back("Flying");//2
	list.push_back("Poison");
	list.push_back("Ground");//4
	list.push_back("Rock");
	list.push_back("Bug");//6
	list.push_back("Ghost");
	list.push_back("Steel");//8
	list.push_back("Fire");
	list.push_back("Water");//10
	list.push_back("Grass");
	list.push_back("Electric");//12
	list.push_back("Psychic");
	list.push_back("Ice");//14
	list.push_back("Dragon");
	list.push_back("Dark");//16
	list.push_back("Fairy");
	list.push_back("");
} // do not modify
void displayTypeList(vector<string> &list)
{
	for (int i = 0; i < list.size(); i++)
	{
		cout << list[i] << endl;
	}
} // do not modify
void displayPokedexList(vector<Pokedex> &list)
{
	for (int i = 0; i < list.size(); i++)
	{
		cout << list[i].name << " - " << list[i].type << " - " << list[i].canEvolve << " - " << list[i].evolveRequirement << endl;
	}
}
void pokedexListCreation(vector<Pokedex> &list)

{
	/*list.push_back(Pokedex());
	list[].name = "";
	list[].type = typeList[];
	list[].canEvolve =
	list[].evolveRequirement =
	*/

	list.push_back(Pokedex());
	list[0].name = "";
	list[0].type = typeList[18];
	list[0].canEvolve = false;
	list[0].evolveRequirement = 100;

	list.push_back(Pokedex());
	list[1].name = "Snivy";
	list[1].type = typeList[11];
	list[1].canEvolve = true;
	list[1].evolveRequirement = 17;

	list.push_back(Pokedex());
	list[2].name = "Servine";
	list[2].type = typeList[11];
	list[2].canEvolve = true;
	list[2].evolveRequirement = 36;

	list.push_back(Pokedex());
	list[3].name = "Serperior";
	list[3].type = typeList[11];
	list[3].canEvolve = false;

	list.push_back(Pokedex());
	list[4].name = "Tepig";
	list[4].type = typeList[9];
	list[4].canEvolve = true;
	list[4].evolveRequirement = 17;

	list.push_back(Pokedex());
	list[5].name = "Pignite";
	list[5].type = typeList[9];
	list[5].canEvolve = true;
	list[5].evolveRequirement = 36;

	list.push_back(Pokedex());
	list[6].name = "Emboar";
	list[6].type = typeList[9];
	list[6].canEvolve = false;

	list.push_back(Pokedex());
	list[7].name = "Oshawott";
	list[7].type = typeList[10];
	list[7].canEvolve = true;
	list[7].evolveRequirement = 17;

	list.push_back(Pokedex());
	list[8].name = "Dewott";
	list[8].type = typeList[10];
	list[8].canEvolve = true;
	list[8].evolveRequirement = 36;

	list.push_back(Pokedex());
	list[9].name = "Samurott";
	list[9].type = typeList[10];
	list[9].canEvolve = false;

	list.push_back(Pokedex());
	list[10].name = "Patrat";
	list[10].type = typeList[0];
	list[10].canEvolve = true;
	list[10].evolveRequirement = 20;

	list.push_back(Pokedex());
	list[11].name = "Watchog";
	list[11].type = typeList[0];
	list[11].canEvolve = false;

	list.push_back(Pokedex());
	list[12].name = "Lillipup";
	list[12].type = typeList[0];
	list[12].canEvolve = true;
	list[12].evolveRequirement = 16;

	list.push_back(Pokedex());
	list[13].name = "Herdier";
	list[13].type = typeList[0];
	list[13].canEvolve = true;
	list[13].evolveRequirement = 32;

	list.push_back(Pokedex());
	list[14].name = "Stoutland";
	list[14].type = typeList[0];
	list[14].canEvolve = false;

	list.push_back(Pokedex());
	list[15].name = "Purrloin";
	list[15].type = typeList[16];
	list[15].canEvolve = true;
	list[15].evolveRequirement = 20;

	list.push_back(Pokedex());
	list[16].name = "Liepard";
	list[16].type = typeList[16];
	list[16].canEvolve = false;
}
void addPokemon(vector<Pokemon> & list, Pokemon & picked)
{
	if (list.size() < 6)
	{
		cout << endl;
		cout << "Pokemon " << picked.name << " was added to the party" << endl;
		list.push_back(picked);
	}
	else
	{
		cout << endl;
		cout << "Pokemon " << picked.name << " cannot be added. It will transfer to the box" << endl;
	}
	system("PAUSE");
}
void partyCheck(vector<Pokemon> & list)
{
	cout << endl;
	cout << "Current Party: " << endl;

	for (int i = 0; i < list.size(); i++)
	{
		list[i].pokemonStatView();
	}
	system("PAUSE");
}
void choosePokemon(vector<Pokemon> & party)
{
	srand(time(0));
	Pokemon * chosen = new Pokemon();
	Pokemon * snivy = new Pokemon(pokedex[1].name, pokedex[1].type, 5, pokedex[1].canEvolve, pokedex[1].evolveRequirement);
	Pokemon * tepig = new Pokemon(pokedex[4].name, pokedex[4].type, 5, pokedex[4].canEvolve, pokedex[4].evolveRequirement);
	Pokemon * oshawott = new Pokemon(pokedex[7].name, pokedex[7].type, 5, pokedex[7].canEvolve, pokedex[7].evolveRequirement);
	int answer;


	system("CLS");
	cout << "Choose from the three pokemon to be your starter" << endl;
	cout << "[1] Snivy || [2] Tepig || [3] Oshawott || [Anything else] Random Pokemon" << endl;
	cout << "choice: ";
	cin >> answer;

	if (answer < 1 || answer > 3)
	{
		answer = rand() % 3 + 1;
	}

	if (answer == 1)
	{
		chosen = snivy;
	}
	if (answer == 2)
	{
		chosen = tepig;
	}
	if (answer == 3)
	{
		chosen = oshawott;
	}

	cout << endl;
	cout << "Congraltulations! You chose " << chosen->name << endl;
	system("PAUSE");

	addPokemon(party, *chosen);
	partyCheck(party);
}
void mapCreation(vector<Area> &list)
{
	list.push_back(Area());
	list[0].name = "Nuvema town";
	list[0].yStart = 0;
	list[0].yEnd = 6;
	list[0].isSafe = true;

	list.push_back(Area());
	list[1].name = "Route 1";
	list[1].yStart = 7;
	list[1].yEnd = 14;
	list[1].isSafe = false;

	list.push_back(Area());
	list[2].name = "Accumula Town";
	list[2].yStart = 15;
	list[2].yEnd = 21;
	list[2].isSafe = true;

	list.push_back(Area());
	list[3].name = "Route 2";
	list[3].yStart = 22;
	list[3].yEnd = 28;
	list[3].isSafe = false;

	list.push_back(Area());
	list[4].name = "Striaton City";
	list[4].yStart = 29;
	list[4].yEnd = 35;
	list[4].isSafe = true;
}

Area areaCheck(vector<Area> &list, int &yLocation)
{
	Area newArea;

	for (int i = 0; i < list.size(); i++)
	{
		if (yLocation >= list[i].yStart && yLocation <= list[i].yEnd)
		{
			newArea = list[i];
		}
	}

	return newArea;
}

void wildEncounter(vector<Pokemon> & list, Player * player, Area &area)
{
	if (area.isSafe == false)
	{
		srand(time(0));
		int odds = rand() % 100;
		int randomPokemon = rand() % 7 + 10;
		int levelOdds = rand() % 5 + 3;
		Pokemon * wildPokemon = new Pokemon(pokedex[randomPokemon].name, pokedex[randomPokemon].type, levelOdds, pokedex[randomPokemon].canEvolve, pokedex[randomPokemon].evolveRequirement);
		if (odds > 30)
		{
			player->encounter(list, wildPokemon);
		}
	}
}



int main()
{
	mapCreation(map);
	typeListCreation(typeList);
	pokedexListCreation(pokedex);
	vector<Pokemon> newParty;
	string answer;
	system("CLS");
	cout << "Welcome to the world. What is your name? ";
	cin >> answer;
	Player* player = new Player(answer);

	cout << endl;
	cout << "Welcome " << player->name << ". Here's your pokeballs and some money" << endl;
	system("PAUSE");
	player->displayPlayer();
	choosePokemon(newParty);

	Area mapArea = map[0];
	while (true)
	{
		srand(time(0));
		system("CLS");
		player->currentLocation(mapArea.name, mapArea.isSafe, newParty);
		player->move(map[map.size() - 1].yEnd);
		mapArea = areaCheck(map, player->yPosition);
		wildEncounter(newParty, player, mapArea);
	}
}