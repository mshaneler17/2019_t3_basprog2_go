#include "Pokemon.h"

Pokemon::Pokemon()
{
	name = "";
	type = "";
	level = 1;
	maxHp = 1;
	currentHp = maxHp;
	goalExp = 100;
	exp = 0;
	expGrowth = 1;
	baseDamage = 1;

	canEvolve = false;
	evolveLevel = 0;
}

Pokemon::Pokemon(string Name, string Type, int Level, bool CanEvolve, int EvolveLevel)
{
	srand(time(0));
	int hpOdds = rand() % 6 + 10;
	int damageOdds = rand() % 6 + 5;

	name = Name;
	type = Type;
	level = Level;

	canEvolve = CanEvolve;
	evolveLevel = EvolveLevel;

	maxHp = hpOdds + addHp(hpOdds);
	currentHp = maxHp;
	baseDamage = damageOdds + addDamage(damageOdds);

	exp = 0;
	goalExp = 100 + addExpRate(100);
	expGrowth = 20 + addExpRate(20);
}

int Pokemon::Damagebonus(Pokemon * target)
{
	if (this->type == "Normal")
	{
		if (target->type == "Rock" || target->type == "Steel") return this->DamageDividesTwo();
		if (target->type == "Ghost") return 0;
		else return this->baseDamage;
	}
	if (this->type == "Fighting")
	{
		if (target->type == "Rock" || target->type == "Steel" || target->type == "Normal" || target->type == "Ice" || target->type == "Dark") return this->DamageTimesTwo();
		if (target->type == "Bug" || target->type == "Fairy" || target->type == "Flying" || target->type == "Poison" || target->type == "Psychic") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Flying")
	{
		if (target->type == "Bug" || target->type == "Fighting" || target->type == "Grass") return this->DamageTimesTwo();
		if (target->type == "Electric" || target->type == "Rock" || target->type == "Steel") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Poison")
	{
		if (target->type == "Fairy" || target->type == "Grass") return this->DamageTimesTwo();
		if (target->type == "Poison" || target->type == "Ground" || target->type == "Rock" || target->type == "Ghost") return this->DamageDividesTwo();
		if (target->type == "Steel") return 0;
		else return this->baseDamage;
	}
	if (this->type == "Ground")
	{
		if (target->type == "Electric" || target->type == "Fire" || target->type == "Poison" || target->type == "Rock" || target->type == "Steel") return this->DamageTimesTwo();
		if (target->type == "Bug" || target->type == "Grass") return this->DamageDividesTwo();
		if (target->type == "Flying") return 0;
		else return this->baseDamage;
	}
	if (this->type == "Rock")
	{
		if (target->type == "Bug" || target->type == "Fire" || target->type == "Flying" || target->type == "Ice") return this->DamageTimesTwo();
		if (target->type == "Fighting" || target->type == "Ground" || target->type == "Steel") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Bug")
	{
		if (target->type == "Dark" || target->type == "Grass" || target->type == "Psychic")  return this->DamageTimesTwo();
		if (target->type == "Fairy" || target->type == "Fighting" || target->type == "Fire" || target->type == "Flying" || target->type == "Ghost" || target->type == "Poison" || target->type == "Steel") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Ghost")
	{
		if (target->type == "Ghost" || target->type == "Psychic") return this->DamageTimesTwo();
		if (target->type == "Dark") return this->DamageDividesTwo();
		if (target->type == "Normal") return 0;
		else return this->baseDamage;
	}
	if (this->type == "Steel")
	{
		if (target->type == "Fairy" || target->type == "Ice" || target->type == "Rock") return this->DamageTimesTwo();
		if (target->type == "Electric" || target->type == "Fire" || target->type == "Steel" || target->type == "Water") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Fire")
	{
		if (target->type == "Bug" || target->type == "Grass" || target->type == "Ice" || target->type == "Steel") return this->DamageTimesTwo();
		if (target->type == "Dragon" || target->type == "Fire" || target->type == "Rock" || target->type == "Water") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Water")
	{
		if (target->type == "Fire" || target->type == "Ground" || target->type == "Rock") return this->DamageTimesTwo();
		if (target->type == "Dragon" || target->type == "Grass" || target->type == "Water") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Grass")
	{
		if (target->type == "Ground" || target->type == "Rock" || target->type == "Water") return this->DamageTimesTwo();
		if (target->type == "Bug" || target->type == "Dragon" || target->type == "Fire" || target->type == "Flying" || target->type == "Grass" || target->type == "Poison" || target->type == "Steel") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Electric")
	{
		if (target->type == "Flying" || target->type == "Water") return this->DamageTimesTwo();
		if (target->type == "Dragon" || target->type == "Electric" || target->type == "Grass") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Psychic")
	{
		if (target->type == "Fighting" || target->type == "Poison") return this->DamageTimesTwo();
		if (target->type == "Psychic" || target->type == "Steel") return this->DamageDividesTwo();
		if (target->type == "Dark") return 0;
		else return this->baseDamage;
	}
	if (this->type == "Ice")
	{
		if (target->type == "Dragon" || target->type == "Flying" || target->type == "Grass" || target->type == "Ground") return this->DamageTimesTwo();
		if (target->type == "Ice" || target->type == "Water") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Dragon")
	{
		if (target->type == "Dragon") return this->DamageTimesTwo();
		if (target->type == "Steel") return this->DamageDividesTwo();
		if (target->type == "Fairy") return 0;
		else return this->baseDamage;
	}
	if (this->type == "Dark")
	{
		if (target->type == "Ghost" || target->type == "Psychic") return this->DamageTimesTwo();
		if (target->type == "Dark" || target->type == "Fairy" || target->type == "Fighting") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	if (this->type == "Fairy")
	{
		if (target->type == "Dark" || target->type == "Dragon" || target->type == "Fighting") return this->DamageTimesTwo();
		if (target->type == "Fire" || target->type == "Poison" || target->type == "Steel") return this->DamageDividesTwo();
		else return this->baseDamage;
	}
	else return this->baseDamage;
}

int Pokemon::addHp(int input)
{
	double add = (input * 0.15) * level;
	return (int)add;
}

int Pokemon::addDamage(int input)
{
	double add = (input * 0.1) * level;
	return (int)add;
}

int Pokemon::addExpRate(int input)
{
	double add = (input * 0.2) * level;
	return (int)add;
}

int Pokemon::DamageTimesTwo()
{
	return this->baseDamage * 2;
}

int Pokemon::DamageDividesTwo()
{
	return this->baseDamage / 2;
}

void Pokemon::pokemonStatView()
{
	cout << endl;
	cout << this->name << " - Level " << this->level << " - " << this->type << endl;
	cout << this->currentHp << " / " << this->maxHp << endl;
}

void Pokemon::attack(Pokemon * target)
{
	srand(time(0));
	int odds = rand() % 100;
	cout << endl;
	cout << this->name << " attacks " << target->name << " with its " << this->type << " attack " << endl;
	cout << "It has the damage of " << this->Damagebonus(target) << endl;

	if (odds < 80)
	{
		cout << this->name << " hit it!" << endl;
		target->currentHp -= this->Damagebonus(target);
	}
	else
	{
		cout << this->name << " missed it!" << endl;
	}

}

void Pokemon::heal()
{
	cout << endl;
	cout << "Your " << this->name << " is healed" << endl;

	this->currentHp = this->maxHp;
}

void Pokemon::getExperience()
{
	system("CLS");
	cout << endl;
	cout << this->name << " earned " << this->expGrowth << " exp!" << endl;
	this->exp += this->expGrowth;
	cout << "EXP: " << this->exp << " / " << this->goalExp << endl;
	system("PAUSE");

	if (this->exp >= this->goalExp)
	{
		this->levelUp();
	}
}

void Pokemon::levelUp()
{
	cout << endl;
	cout << this->name << " level up!" << endl;

	this->level++;
	this->maxHp += this->addHp(this->maxHp);
	this->currentHp = maxHp;

	this->baseDamage += this->addDamage(this->baseDamage);
	this->exp = 0;
	this->expGrowth += this->addExpRate(this->expGrowth);
	this->goalExp += this->addExpRate(this->expGrowth);

	this->pokemonStatView();
	system("PAUSE");
}
